import requests
import json
import urllib3
import time

class CPAPI:
    
    def __init__(self, config = None, messages = None):
        self.config = config
        self.div = "|:" + "-"*40
        self.cpadmin = self.config["cp_mgmt_user"]
        self.centralgw = config["vpn_central_gw"]
        self.satellitegw = config["vpn_satellite_gw"]
        self.vpnname = config["vpn_name"]
        self.msgs = messages
        self.api_url = "https://" + self.config['cp_mgmt_host'] + '/web_api/'
        self.headers = { 'Content-Type':'application/json' }
        urllib3.disable_warnings()
        
    def show_config(self):
        print(self.config)
        
    def apiPost(self, endpoint = None, query = None):
        apiURL = self.api_url + endpoint

        try:
            response = requests.post(apiURL, data = json.dumps(query), headers = self.headers, verify = False)
            response = json.loads(response.text)
            return response
        except:
            print(self.msgs["api_error"])
            print(response.text)
            return 0
        
      
    
    def login(self):
        ##- Check if session.json exist
        print(self.msgs["api_last_session"])
        try:
            with open('session.json') as session_file:
                api_session = json.load(session_file)
            session_id = api_session['sid']
            self.headers['X-chkp-sid'] = session_id
            print(self.headers)
            print(self.div)
            print(self.msgs["api_last_session_sid"] % session_id)
            print(api_session)
            ##- Loging into api
            print(self.div)
        except:
            print(self.msgs["api_login"])
            query = { "user":self.cpadmin,
                  "password":self.config["cp_mgmt_password"] }
            response = self.apiPost(endpoint = "login", query = query)
            self.headers['X-chkp-sid'] = response['sid']
            print(response)
            print(self.headers)

            ##- Save session
            with open('session.json', 'w') as session_file:
                json.dump(response, session_file)
            ##- Remove management lock
            #print(self.msgs["unlock"])
            #query = { "name" : self.cpadmin }
            #response = self.apiPost(endpoint = "unlock-administrator", query = query)
            #print(response)
            print(self.div)
            
    def doStarVPN(self):
        print(self.msgs["vpn_create_init"])
        query = { "name" : self.vpnname,
                 "center-gateways": self.centralgw,
                 "satellite-gateways": self.satellitegw }
        
        response = self.apiPost(endpoint = "add-vpn-community-star", query = query)
        print(response)
        print(self.div)
        
    def doAccesRule(self):
        print(self.msgs["api_add_rule"])
        query = { "layer":"Network",
                 "name" : self.vpnname + " VPN Access",
                 "action" :"Accept",
                 "position": "top",
                 "source": self.config["rule_source"],
                 "destination": self.config["rule_destination"],
                 "vpn": self.vpnname,
                 "track":"Log"}
        
        response = self.apiPost(endpoint = "add-access-rule", query = query)
        print(response)
        time.sleep(3)
        print(self.div)
        
    
    def publish(self):
        print(self.msgs["api_publish"])
        query = { }
        response = self.apiPost(endpoint = "publish", query = query)
        print(response)
        #Wait for the the policy to be really published
        time.sleep(3)
        print(self.div)
        
        
    def install(self):
        print(self.msgs["api_install_policy"])
        query = { "policy-package":"Standard" }
        response = self.apiPost(endpoint = "install-policy", query = query)
        print(response)
        print(self.div)
            
        
    
        
        
        
        