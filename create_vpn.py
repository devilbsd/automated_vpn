##- Automated VPN between 2 CP devices managed by the same console using R80 Management API

##- Javier Padilla - 02/11/2020
##- jpadilla@checkpoint.com

import json
import cpapi


with open('config.json') as config_file:
    config = json.load(config_file)
    
with open('messages.json') as messages_file:
    messages = json.load(messages_file)


cp_session = cpapi.CPAPI(config, messages)

cp_session.login()
cp_session.doStarVPN()
cp_session.doAccesRule()
cp_session.publish()
cp_session.install()
